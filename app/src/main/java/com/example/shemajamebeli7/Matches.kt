package com.example.shemajamebeli7

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.shemajamebeli7.databinding.FragmentMatchesBinding
import com.example.shemajamebeli7.adapter.RecyclerAdapter

class Matches : Fragment() {

    private lateinit var binding : FragmentMatchesBinding

    private  val viewModel: ViewModel by viewModels()

    private lateinit var adapter : RecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMatchesBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        viewModel.init()
        recycler()
        observes()
    }

    private fun recycler(){
        adapter = RecyclerAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter
    }

    private fun observes(){
        viewModel._itemsLiveData.observe(viewLifecycleOwner, Observer {
            val  matchInfo = it.match?.stadiumAdress.toString()+" "+it.match?.matchDate.toString()
            val score = it.match?.team1?.score.toString() + ":"+it.match?.team2?.score.toString()
            binding.date.text = matchInfo
            binding.score.text = score
            binding.Time.text = it.match?.matchTime.toString()
            glide(it.match?.team1?.teamImage.toString(),binding.team1Flag)
            glide(it.match?.team2?.teamImage.toString(),binding.team2Flag)
            binding.team1Name.text = it.match?.team1?.teamName.toString()
            binding.team2Name.text = it.match?.team2?.teamName.toString()
            it.match?.matchSummary?.summaries?.let { it1 -> adapter.getSummery(it1) }
        })
    }

    private fun glide(url:String,image : ImageView){
        Glide.with(this)
            .load(url).placeholder(R.drawable.hellokitty)
            .into(image)
    }

}