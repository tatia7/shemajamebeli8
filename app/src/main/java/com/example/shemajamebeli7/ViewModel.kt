package com.example.shemajamebeli7

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shemajamebeli7.Models.Moedl
import com.example.shemajamebeli7.api.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ViewModel : ViewModel() {

    private val itemsLiveData = MutableLiveData<Moedl>().apply {
        mutableListOf<Moedl>()
    }

    val _itemsLiveData : MutableLiveData<Moedl> = itemsLiveData

    fun init(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getItems()
            }
        }

    }

    private suspend fun  getItems(){


        val itemsList = RetrofitService.RetrofitService().getList()
        if (itemsList.isSuccessful){
            val items = itemsList.body()
            Log.d("message", "${items}")
            itemsLiveData.postValue(items)
        }else {
            itemsList.code()
        }
    }

}