package com.example.shemajamebeli7.adapter



import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeli7.Models.Summary
import com.example.shemajamebeli7.databinding.RecyclerLayoutBinding


class RecyclerAdapter():RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

   private var summery = mutableListOf<Summary>()

   inner class ViewHolder( val binding : RecyclerLayoutBinding):RecyclerView.ViewHolder(binding.root){

       private lateinit var fisrtTeamAdapter : FirstTeamRecyclerAdapter
       private  lateinit var SecondTeamAdapter : SecondTeamRecyclerAdapter

       fun bind(){
           FirstRecycler()
           SecondRecycler()
       }
       fun FirstRecycler(){
           fisrtTeamAdapter = FirstTeamRecyclerAdapter(summery[adapterPosition])
           binding.firstTeamRecycler.layoutManager = LinearLayoutManager(binding.root.context)
           binding.firstTeamRecycler.adapter = fisrtTeamAdapter
       }
       fun SecondRecycler(){

           SecondTeamAdapter = SecondTeamRecyclerAdapter(summery[adapterPosition])
           binding.SecondTeamRecycler.layoutManager = LinearLayoutManager(binding.root.context)
           binding.SecondTeamRecycler.adapter = SecondTeamAdapter
       }
   }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                RecyclerLayoutBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount()=summery.size

    fun getSummery(summeries: List<Summary>){
        this.summery = summeries as MutableList<Summary>
        notifyDataSetChanged()
    }
}