package com.example.shemajamebeli7.Models

data class Team2(
    val ballPosition: Int?,
    val score: Int?,
    val teamImage: String?,
    val teamName: String?
)