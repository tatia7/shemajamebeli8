package com.example.shemajamebeli7.Models

data class Team2Action(
    val action: Action?,
    val actionType: Int?,
    val teamType: Int?
)