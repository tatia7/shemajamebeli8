package com.example.shemajamebeli7.Models

data class Player(
    val playerImage: String?,
    val playerName: String?
)