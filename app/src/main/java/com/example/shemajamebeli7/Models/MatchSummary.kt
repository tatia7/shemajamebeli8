package com.example.shemajamebeli7.Models

data class MatchSummary(
    val summaries: List<Summary>?
)